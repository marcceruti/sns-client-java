package hello;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateTopicDTO {

	private String userId;
	private String phoneNumber;
	private String emailAddress;
	
}
