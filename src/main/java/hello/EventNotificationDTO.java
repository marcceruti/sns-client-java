package hello;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import hello.SNSService.SNSMessage;
import hello.SNSService.SubscriptionProtocol;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventNotificationDTO {

    private String userId;
    private SNSMessage snsMessage;
    private SubscriptionProtocol[] additionalSubscribers;

    @Data
    public static class CTA{
        private Integer index;
        private String text;
        private String ctaType;
        private Object ctaArgs;
    }

}
