package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class SNSController {
	
	private SNSService snsService;
	
	@Autowired
	public SNSController(SNSService snsService) {
		this.snsService = snsService;
	}
    
    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }
    
    @RequestMapping(method= RequestMethod.POST, value="/alert-subscribers")
    public void alertSubscribers(@RequestBody EventNotificationDTO eventNotificationDTO) {
        snsService.alertSubscribers(eventNotificationDTO);
    }
    
    @RequestMapping(method= RequestMethod.POST, value="/create-topic")
    public void alertSubscribers(@RequestBody CreateTopicDTO createTopicDTO) {
        snsService.createTopic(createTopicDTO);
    }
    
}
