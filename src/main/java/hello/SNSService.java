package hello;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.ListSubscriptionsByTopicResult;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.Subscription;

import lombok.Data;

@Service
public class SNSService {
	
	private static final String SOCKET_HUB_URL = "http://marchackathonvirginia-env.w4dcuj4ik2.us-east-1.elasticbeanstalk.com/message";
	private static final String ARN_PREFIX = "arn:aws:sns:us-east-1:651686769409:";
	private AmazonSNS snsClient;
	
	public SNSService() {
		snsClient = AmazonSNSClientBuilder.defaultClient();
	}
	
	public enum SubscriptionProtocol {
		HTTP("http"),
		HTTPS("https"),
		EMAIL("email"),
		EMAIL_JSON("email-json"),
		SMS("sms"),
		SQS("sqs"),
		APPLICATION("application"),
		LAMBDA("lambda");
		
		String val;
		
		SubscriptionProtocol(String val) {
			this.val = val;
		}
		
		public static SubscriptionProtocol fromVal(String val) {
			switch (val) {
				case "http":
					return SubscriptionProtocol.HTTP;
				case "https":
					return SubscriptionProtocol.HTTPS;
				case "email":
					return SubscriptionProtocol.EMAIL;
				case "email-json":
					return SubscriptionProtocol.EMAIL_JSON;
				case "sms":
					return SubscriptionProtocol.SMS;
				case "sqs":
					return SubscriptionProtocol.SQS;
				case "application":
					return SubscriptionProtocol.APPLICATION;
				case "lambda":
					return SubscriptionProtocol.LAMBDA;
				default: 
					throw new RuntimeException("Unrecognized protocol: "+val);
			}
		}
	}

	public void createTopic(CreateTopicDTO createTopicDTO) {
        CreateTopicRequest createTopicRequest = new CreateTopicRequest(createTopicDTO.getUserId());
        CreateTopicResult createTopicResult = snsClient.createTopic(createTopicRequest);
        System.out.println(createTopicResult);
        System.out.println("CreateTopicRequest - " + snsClient.getCachedResponseMetadata(createTopicRequest));
        String topicArn = createTopicResult.getTopicArn();
        subscribeToTopic(topicArn, SubscriptionProtocol.EMAIL, createTopicDTO.getEmailAddress());
        subscribeToTopic(topicArn, SubscriptionProtocol.HTTP, SOCKET_HUB_URL);
        subscribeToTopic(topicArn, SubscriptionProtocol.SMS, createTopicDTO.getPhoneNumber());
        // TODO: add DB write subscriber (pass token along) --> won't work because dev is walled off
	}
	
	public void subscribeToTopic(String topicArn, SubscriptionProtocol subscriptionProtocol, String endpoint) {
		SubscribeRequest subRequest = new SubscribeRequest(topicArn, subscriptionProtocol.val, endpoint);
		snsClient.subscribe(subRequest);
	}

	public void alertSubscribers(EventNotificationDTO eventNotificationDTO) {
		publishTopic(eventNotificationDTO.getUserId(), eventNotificationDTO.getSnsMessage(), eventNotificationDTO.getAdditionalSubscribers());
	}
	
	@Data
	public static class SNSMessage {
	    private String email;
	    private String smsMessage;
	    private String inAppNotificationMessage;
	    
	    public String toJSON() {
	    	JSONObject json = new JSONObject();
    		json.put("default", inAppNotificationMessage);
	    	if (email != null) {
	    		json.put(SubscriptionProtocol.EMAIL.val, email);
	    	}
	    	if (smsMessage != null) {
	    		json.put(SubscriptionProtocol.SMS.val, smsMessage);
	    	}
	    	return json.toString();
	    }
	}
	
	public void publishTopic(String userId, SNSMessage message, SubscriptionProtocol[] additionalSubscribers) {
		initSubscriptionFilters(userId); // this can only be done after user confirms subscription
		// we "cheat" and set the attributes on every publishTopic()
		// really we would only want to do this once
		
		//publish to an SNS topic
		PublishRequest publishRequest = new PublishRequest(getUserTopicArn(userId), message.toJSON());
		publishRequest.setMessageStructure("json");
		addSubscriptionProtocolFilters(additionalSubscribers, publishRequest);
		PublishResult publishResult = snsClient.publish(publishRequest);
		System.out.println("MessageId - " + publishResult.getMessageId());
	}

	public void addSubscriptionProtocolFilters(SubscriptionProtocol[] additionalSubscribers,
			PublishRequest publishRequest) {
		Map<String, MessageAttributeValue> messageAttributes = new HashMap<>();
		MessageAttributeValue additionalSubscribersMessageAttribute = new MessageAttributeValue();
		additionalSubscribersMessageAttribute.setDataType("String.Array");
		JSONArray additionalSubscribersJson = new JSONArray();
		additionalSubscribersJson.put(SubscriptionProtocol.HTTP.val); 
		if (additionalSubscribers != null) {
			for (SubscriptionProtocol additionalSubscriber : additionalSubscribers) {
				if (additionalSubscriber != SubscriptionProtocol.HTTP) { // HTTP is always included, since this is in-app notification
					additionalSubscribersJson.put(additionalSubscriber.val);
				}
			}
		}
		additionalSubscribersMessageAttribute.setStringValue(additionalSubscribersJson.toString());
		messageAttributes.put("protocol", additionalSubscribersMessageAttribute);
		publishRequest.setMessageAttributes(messageAttributes);
	}

	private void initSubscriptionFilters(String userId) {
		ListSubscriptionsByTopicResult subscriptions = snsClient.listSubscriptionsByTopic(getUserTopicArn(userId));
		List<Subscription> _subscriptions = subscriptions.getSubscriptions();
		_subscriptions.stream().forEach(subscription -> {
			String subscriptionArn = subscription.getSubscriptionArn();
			SubscriptionProtocol protocol = SubscriptionProtocol.fromVal(subscription.getProtocol());
			addSubcriptionProtocolFilter(subscriptionArn, protocol);
		});
	}

	public String getUserTopicArn(String userId) {
		return ARN_PREFIX + userId;
	}
	
	private void addSubcriptionProtocolFilter(String subscriptionArn, SubscriptionProtocol subscriptionProtocol) {
		try {
			JSONObject filterPolicy = new JSONObject();
			JSONArray protocol = new JSONArray();
			protocol.put(subscriptionProtocol.val);
			filterPolicy.put("protocol", protocol);
			snsClient.setSubscriptionAttributes(subscriptionArn, "FilterPolicy", filterPolicy.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
